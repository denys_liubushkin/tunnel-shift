﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesSpawner : MonoBehaviour {
    public GameObject enemyPrefab;
    public TunnelCurve tunnelCurve;
    private DifficultyManager _diffMngr;

    private void Start() {
        _diffMngr = GameObject.Find("Difficulty Manager").GetComponent<DifficultyManager>();
        Invoke("SpawnEnemy", 2);
    }

    private void SpawnEnemy() {
        if (Random.value < 0.5) {
            var enemy = Instantiate(enemyPrefab);
            enemy.GetComponent<TunnelObject>().tunnelPosition.z = tunnelCurve.segmentLength * tunnelCurve.segmentsNumber - 0.2f;
            enemy.GetComponent<Enemy>().tunnelCurve = tunnelCurve;
        }
        Invoke("SpawnEnemy", _diffMngr.enemySpawnInterval);
    }
}
