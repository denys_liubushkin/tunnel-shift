﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    public float cooldown = 1;
    public GameObject progressFill;
    public Transform arrowOrigin;
    public GameObject arrowPrefab;
    public GameObject cameraObject;

    private float shotStrength = 30;
    private float torqueValue = 80;
    private float lastShotTime = 0;

    private void Start() {
        GameManager.instance.state = GameState.Playing;
        GameManager.instance.currentGameScore = 0;
    }

    private void Update() {
        if (Input.GetButton("Fire1")) {
            ShootArrow();
        }
        UpdateProgressBar();
    }

    private void UpdateProgressBar() {
        progressFill.GetComponent<Image>().fillAmount = (Time.timeSinceLevelLoad - lastShotTime) / cooldown;
    } 

    private void ShootArrow() {
        if (GameManager.instance.state != GameState.Playing || Time.timeSinceLevelLoad - lastShotTime < cooldown) return;

        var inputRay = cameraObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        
        var arrow = Instantiate(arrowPrefab, arrowOrigin, false);
        arrow.transform.rotation = Quaternion.LookRotation(inputRay.direction);
        arrow.GetComponent<Rigidbody>().AddForce(arrow.transform.forward * shotStrength, ForceMode.Impulse);
        arrow.GetComponent<Rigidbody>().AddTorque(arrow.transform.forward * torqueValue, ForceMode.Impulse);
        lastShotTime = Time.timeSinceLevelLoad;
    }
}
