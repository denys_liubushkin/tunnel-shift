﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static GameManager instance = null;

    public GameState state = GameState.MainMenu;
    public float shiftingSpeed = 0.5f;
    public uint bestScore = 0;
    public uint currentGameScore = 0;

    private void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void StartGame() {
        currentGameScore = 0;
        SceneManager.LoadScene("Game Scene");
        state = GameState.Playing;
    }

    public void RestartGame() {
        currentGameScore = 0;
        SceneManager.LoadScene("Game Scene");
        ResumeGame();
    }

    public void PauseGame() {
        state = GameState.Paused;
        Time.timeScale = 0;
    }

    public void ResumeGame() {
        state = GameState.Playing;
        Time.timeScale = 1;
    }

    public void DisplayGameOverScreen() {
        var gameOverCanvas = GameObject.Find("GameOver Canvas");
        gameOverCanvas.GetComponent<Canvas>().enabled = true;
        PauseGame();
        gameOverCanvas.transform.Find("Panel/Score").GetComponent<Text>().text = "Score: " + currentGameScore;
        gameOverCanvas.transform.Find("Panel/Best Score").GetComponent<Text>().text = "Best score: " + bestScore;
    }
}

public enum GameState {
    Playing, Paused, MainMenu
}
