﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyManager : MonoBehaviour {
    public float rotationSpeed = 0.25f;
    public float rotationRange = 2.5f;
    public float perlinXOffset = 0.4f;
    public float enemySpawnInterval = 4;

    private void Update() {
        RaiseDifficulty();
    }

    private void RaiseDifficulty() {
        rotationSpeed += 0.01f * Time.deltaTime;
        rotationSpeed = Mathf.Min(rotationSpeed, 1.25f);

        rotationRange += 0.1f * Time.deltaTime;
        rotationRange = Mathf.Min(rotationRange, 12);

        perlinXOffset -= 0.0001f * Time.deltaTime;
        perlinXOffset = Mathf.Max(perlinXOffset, 0.04f);

        GameManager.instance.shiftingSpeed += 0.01f * Time.deltaTime;
        GameManager.instance.shiftingSpeed = Mathf.Min(GameManager.instance.shiftingSpeed, 10);

        enemySpawnInterval -= 0.025f * Time.deltaTime;
        enemySpawnInterval = Mathf.Max(enemySpawnInterval, 0.75f);
    }
}
