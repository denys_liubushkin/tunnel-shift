﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : TunnelObject {
    public TunnelCurve tunnelCurve;
    public Text scoreText;
    private bool gameOverDisplayed = false;

    private void Start() {
        scoreText = GameObject.Find("Score Text").GetComponent<Text>();
    }

    private void Update() {
        tunnelPosition.z -= GameManager.instance.shiftingSpeed * Time.deltaTime;
    }

    private void LateUpdate() {
        if (tunnelPosition.z < 3 && !gameOverDisplayed) {
            GameManager.instance.DisplayGameOverScreen();
            gameOverDisplayed = true;
        }
        else {
            SnapToTunnel();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Projectile")) {
            AddScoreFromHit();
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }

    private void AddScoreFromHit() {
        GameManager.instance.currentGameScore += Convert.ToUInt32(Mathf.RoundToInt(tunnelPosition.z) * Time.timeSinceLevelLoad);
        GameManager.instance.bestScore = Math.Max(GameManager.instance.bestScore, GameManager.instance.currentGameScore);
        scoreText.text = GameManager.instance.currentGameScore.ToString();
    }

    private void SnapToTunnel() {
        transform.localPosition = tunnelCurve.GetPositionOnCurve(tunnelPosition);
        var upPoint = tunnelPosition;
        upPoint.y = upPoint.y + 0.5f;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, tunnelCurve.GetPositionOnCurve(upPoint) - transform.localPosition);
    }
}
