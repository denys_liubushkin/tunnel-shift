﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    public uint lifeTime = 10;

    private void Awake() {
        Invoke("DestroyArrow", lifeTime);
    }

    private void DestroyArrow() {
        Destroy(gameObject);
    }
}
