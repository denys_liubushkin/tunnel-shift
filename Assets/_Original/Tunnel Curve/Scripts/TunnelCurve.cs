﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains and manages curve nodes
/// </summary>
public class TunnelCurve : MonoBehaviour {
    public GameObject nodePrefab;
    public GameObject segmentPrefab;
    public List<GameObject> nodes;
    public float segmentLength = 2;
    public uint segmentsNumber = 4;
    public uint staticSegmentsNumber = 2;

    private void Start() {
        GameObject parentNode = AddNode(gameObject, false);

        for (int i = 0; i < staticSegmentsNumber; i++) {
            parentNode = AddNode(parentNode, false);
        }

        for (uint i = 0; i < segmentsNumber - staticSegmentsNumber; i++) {
            parentNode = AddNode(parentNode, true);
            parentNode.GetComponent<CurveNode>().index = i;
        }
    }

    private GameObject AddNode(GameObject parent, bool canRotate) {
        var position = parent.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0, 0, segmentLength));
        GameObject node = Instantiate(nodePrefab, position, Quaternion.identity, parent.transform);
        node.GetComponent<CurveNode>().canRotate = canRotate;
        nodes.Add(node);
        return node;
    }

    /// <summary>
    /// Translates position on straight tunnel to current tunnel curve
    /// </summary>
    /// <param name="pointOnIdentity">Point position on straight tunnel</param>
    /// <returns>Point position on tunnel curve</returns>
    public Vector3 GetPositionOnCurve(Vector3 pointOnIdentity) {
        int nodeIndex = Mathf.FloorToInt(pointOnIdentity.z / segmentLength);
        GameObject node = nodes[nodeIndex];
        GameObject childNode = nodes[nodeIndex + 1];
        var pointA = node.transform.localToWorldMatrix.MultiplyPoint3x4(new Vector3(pointOnIdentity.x, pointOnIdentity.y));
        var pointB = childNode.transform.localToWorldMatrix.MultiplyPoint3x4(new Vector3(pointOnIdentity.x, pointOnIdentity.y));
        var segmentVector = pointB - pointA;
        //Debug.DrawLine(pointA, pointB, Color.red, 3);
        return pointA + segmentVector * ((pointOnIdentity.z % segmentLength) / segmentLength);
    }
}