﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurveNode : MonoBehaviour {
    public uint index;
    public bool canRotate;

    private DifficultyManager _diffMngr;
    private float _perlinYOffset = 300;

    private void Start() {
        _diffMngr = GameObject.Find("Difficulty Manager").GetComponent<DifficultyManager>();
    }

    private void Update() {
        if (canRotate) {
            UpdateNodeRotation();
        }
    }

    private void UpdateNodeRotation() {
        float perlinX = Time.time * _diffMngr.rotationSpeed + _diffMngr.perlinXOffset * index;

        float randomAngleY = Mathf.PerlinNoise(perlinX, 2 * _perlinYOffset);
        float randomAngleX = Mathf.PerlinNoise(perlinX, 1 * _perlinYOffset);
        float randomAngleZ = Mathf.PerlinNoise(perlinX, 3 * _perlinYOffset);
        float rotationValueX = Mathf.Lerp(-_diffMngr.rotationRange, _diffMngr.rotationRange, randomAngleX);
        float rotationValueY = Mathf.Lerp(-_diffMngr.rotationRange, _diffMngr.rotationRange, randomAngleY);
        float rotationValueZ = Mathf.Lerp(-_diffMngr.rotationRange, _diffMngr.rotationRange, randomAngleZ);

        transform.localRotation = Quaternion.Euler(rotationValueX, rotationValueY, rotationValueZ);
    }
}
