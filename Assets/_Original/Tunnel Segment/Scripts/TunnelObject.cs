﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents translation on a straight tunnel
/// </summary>
public class TunnelObject : MonoBehaviour {
    public Vector3 tunnelPosition;

    private void Update() {
        tunnelPosition.z -= GameManager.instance.shiftingSpeed * Time.deltaTime;
    }
}
