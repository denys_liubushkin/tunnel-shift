﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible for random tunnel segment generation
/// </summary>
public class SegmentGenerator {
    private GameObject _segmentPrefab = Resources.Load("Test segment") as GameObject;
    private int _texturesWidthNumber = 1;

    /// <summary>
    /// Instantiates random tunnel segment
    /// </summary>
    /// <param name="parent">Parent that will hold instantiated tunnel segment</param>
    /// <returns>Instantiated segment</returns>
    public GameObject InstantiateRandomSegment(Transform parent) {
        GameObject result = Object.Instantiate(_segmentPrefab, parent);
        SetRandomTexture(result);
        return result;
    }

    /// <summary>
    /// Sets random texture to segment by modifying it's uvs
    /// </summary>
    /// <param name="segment">Segment to be modified</param>
    private void SetRandomTexture(GameObject segment) {
        MeshFilter meshFilter = segment.GetComponent<MeshFilter>();
        float offset = 1.0f / _texturesWidthNumber;
        offset *= Mathf.FloorToInt(Random.Range(0, 2));
        Vector2[] uvs = meshFilter.mesh.uv;
        for (int i = 0; i < uvs.Length; i++) {
            uvs[i].x += offset;
        }
        meshFilter.mesh.uv = uvs;
    }
}
