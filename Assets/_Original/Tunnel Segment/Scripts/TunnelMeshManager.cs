﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages tunnel walls mesh
/// </summary>
public class TunnelMeshManager : MonoBehaviour {
    private TunnelObject _lastSegment;
    public TunnelCurve tunnelCurve;
    private float _segmentDepth = 2;
    private MeshFilter _meshFilter;
    private SegmentGenerator _segmentGenerator;

    private void Start() {
        _meshFilter = GetComponent<MeshFilter>();
        _segmentGenerator = new SegmentGenerator();
    }

    private void LateUpdate() {
        DestroyRedundantSegments();
        PlaceSegment();
        CombineMeshes();
        SnapMeshToTunnel();
    }

    /// <summary>
    /// Destroys segments that reach curve beginning
    /// </summary>
    private void DestroyRedundantSegments() {
        for (int i = 0; i < transform.childCount; i++) {
            var child = transform.GetChild(i).gameObject;
            if (child.GetComponent<TunnelObject>().tunnelPosition.z <= _segmentDepth * 0.5f) {
                DestroyImmediate(child, false);
            }
        }
    }

    /// <summary>
    /// Places segment at the beginning of the curve
    /// </summary>
    private void PlaceSegment() {
        if (_lastSegment == null) {
            _lastSegment = _segmentGenerator.InstantiateRandomSegment(transform).GetComponent<TunnelObject>();
            _lastSegment.tunnelPosition.z = tunnelCurve.segmentLength * tunnelCurve.segmentsNumber - _segmentDepth;
            _lastSegment.GetComponent<MeshRenderer>().enabled = false;
        }
        else if (_lastSegment.tunnelPosition.z < tunnelCurve.segmentLength * tunnelCurve.segmentsNumber - (_segmentDepth * 2)) {
            float newPosition = _lastSegment.tunnelPosition.z + _segmentDepth;
            _lastSegment = _segmentGenerator.InstantiateRandomSegment(transform).GetComponent<TunnelObject>();
            _lastSegment.tunnelPosition.z = newPosition;
            _lastSegment.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    /// <summary>
    /// Combines children of mesh manager into it's mesh
    /// </summary>
    private void CombineMeshes() {
        CombineInstance[] combines = new CombineInstance[transform.childCount];
        for (int i = 0; i < transform.childCount; i++) {
            var child = transform.GetChild(i);
            combines[i].mesh = child.GetComponent<MeshFilter>().mesh;
            combines[i].transform = Matrix4x4.TRS(child.GetComponent<TunnelObject>().tunnelPosition, Quaternion.identity, Vector3.one);
        }
        _meshFilter.mesh = new Mesh();
        _meshFilter.mesh.CombineMeshes(combines, true, true);
    }

    /// <summary>
    /// Places vertices of mesh to the tunnel curve
    /// </summary>
    private void SnapMeshToTunnel() {
        Vector3[] vertices = _meshFilter.mesh.vertices;

        for (int i = 0; i < vertices.Length; i++) {
            vertices[i] = tunnelCurve.GetPositionOnCurve(vertices[i]);
        }

        _meshFilter.mesh.vertices = vertices;
    }
}
