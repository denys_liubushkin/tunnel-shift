# README #

## [Online Demo](https://sunsets-chaser.itch.io/tunnel-shift?secret=g6JkfiAd5c5WjGDwYgSyAK8fIs) ##

## Summary ##
Endless runner game for mobile platforms.

## Gameplay ##
Tap/click on game screen to shoot projectile.
Shoot bats to advance. The further the killed bat is from you, the more score points you get. 

## Attributions ##
[Bat model](https://sketchfab.com/models/2d0117f0f69f421db5be4af20c20e93f) by [BitGem](https://sketchfab.com/bitgem)

## Screenshots ##
![Screenshot1](https://bitbucket.org/repo/jeg9Rg/images/1715158694-Screenshot3.png)
![Screenshot2](https://bitbucket.org/repo/jeg9Rg/images/2877616441-Screenshot15.png)